#include <stdint.h>
#include <string.h>

#include <Wire.h>

#include "raat.hpp"
#include "raat-buffer.hpp"

#include "raat-oneshot-timer.hpp"
#include "raat-oneshot-task.hpp"
#include "raat-task.hpp"

#include "http-get-server.hpp"

static const raat_devices_struct * s_pDevices = NULL;
static bool all_puzzle_inputs_on = false;
static bool final_input_on = false;

static void send_standard_erm_response(HTTPGetServer& server)
{
    server.set_response_code_P(PSTR("200 OK"));
    server.set_header_P(PSTR("Access-Control-Allow-Origin"), PSTR("*"));
    server.finish_headers();
}

static void get_part(HTTPGetServer& server, uint8_t part)
{
    send_standard_erm_response(server);
    server.add_body_P(s_pDevices->pInputs[part-1]->state() ? PSTR("COMPLETE\r\n\r\n") : PSTR("NOT COMPLETE\r\n\r\n"));
}

static void get_part(HTTPGetServer& server, char const * const url, char const * const additional)
{
    (void)url;
    uint8_t input_number = 255;

    if (raat_parse_single_numeric(additional, input_number, NULL) && inrange(input_number, (uint8_t)1, (uint8_t)4))
    {
        get_part(server, input_number);
    }
}

static const char PART_GET_URL[] PROGMEM = "/part/status";

static http_get_handler s_handlers[] = 
{
    {PART_GET_URL, get_part},
    {"", NULL}
};
static HTTPGetServer s_server(NULL);

void ethernet_packet_handler(char * req)
{
    s_server.handle_req(s_handlers, req);
}

char * ethernet_response_provider()
{
    return s_server.get_response();
}

static void status_task_fn(RAATTask& task, void * pTaskData)
{
    (void)task; (void)pTaskData;
    for (uint8_t i = 0; i<s_pDevices->InputsCount; i++)
    {
        raat_logln_P(LOG_APP, PSTR("Part %u: %S"),
            i,
            s_pDevices->pInputs[i]->state() ? PSTR("1") : PSTR("0")
        );
    }
}
static RAATTask s_status_task(1000, status_task_fn);

static void input_scan_task_fn(RAATTask& task, void * pTaskData)
{
    (void)task; (void)pTaskData;
    
    all_puzzle_inputs_on = true;

    for (uint8_t i = 0; i<s_pDevices->InputsCount; i++)
    {
        all_puzzle_inputs_on &= s_pDevices->pInputs[i]->state();
    }

    s_pDevices->pFinalLEDOutput->set(all_puzzle_inputs_on);

    final_input_on = s_pDevices->pFinalSwitchInput->state() == false;

    if (all_puzzle_inputs_on && final_input_on)
    {
        s_pDevices->pMaglockOutput->set(false);        
        s_pDevices->pSSROutput->set(true);
    }
    else if (!s_pDevices->pMaglockOutput->state() && s_pDevices->pInputs[0]->state() == false)
    {
        // Turn on maglock when already on and when power is lost (this is a game reset)
        s_pDevices->pMaglockOutput->set(true);  
	    s_pDevices->pSSROutput->set(false);
    }
}

static RAATTask s_input_scan_task(50, input_scan_task_fn);

void raat_custom_setup(const raat_devices_struct& devices, const raat_params_struct& params)
{
    (void)params;
    s_pDevices = &devices;
}

void raat_custom_loop(const raat_devices_struct& devices, const raat_params_struct& params)
{
    (void)devices; (void)params;
    s_status_task.run();
    s_input_scan_task.run();
}

int raat_application_serial_handler(PARAM_ADDRESS address, char const * const command, char * reply)
{
    bool handled = s_server.handle_req(s_handlers, command);
    if (handled)
    {
        strcpy(reply, "OK");
    }
    else
    {
        strcpy(reply, "URL?");
    }
    return strlen(reply);
}
